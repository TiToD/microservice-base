import unittest
from unittest import mock
import requests
from requests.exceptions import ConnectionError
import requests_mock
from bugzilla import MyBugzilla
import json

class TestBugzilla(unittest.TestCase):

    def test_bug_id(self):
        zilla = MyBugzilla("zaem087@gmail.com",
                           "http://example.com")
        link = zilla.bug_link("12345")
        self.assertEqual(
                link,
                "http://example.com/show/bug.cgi?id=12345"
        )

    @requests_mock.Mocker()
    def test_get_new_bugs(self, mocker):
        bugs = {
            "bugs": [
                {"id": 123},
                {"id": 456}
                ]
            }

        mocker.get(requests_mock.ANY, json=bugs)

        zilla = MyBugzilla("zaem087@gmail.com",
                           "http://example.com")
        links = zilla.get_new_bugs()
        self.assertEqual(links[0], "http://example.com/show/bug.cgi?id=123")

    @mock.patch.object(requests.Session, 'get',
                       side_effect=ConnectionError('No network'))
    def test_network_error(self, mocker):
        zilla = MyBugzilla("zaem087@gmail.com", "http://example.com")

        bugs = zilla.get_new_bugs()
        self.assertEqual(
                bugs[0],
                "http://example.com/show/bug.cgi?id=0000"
        )


if __name__ == "__main__":
    unittest.main()
