import unittest
from unittest import mock
import json
# from flask.testing import FlaskClient
from app import app as tested_app

_404 = ('The requested URL was not found on the server. '
        'If you entered the URL manually please check your '
        'spelling and try again.')


class TestApp(unittest.TestCase):
    def setUp(self):
        self.app = tested_app.test_client()

    def test_api(self):
        resp = self.app.get('/api')
        resp = json.loads(resp.data.decode("utf-8"))
        self.assertEqual(resp, {"Hello": "World"})

# #     @mock.patch.object(FlaskClient, "route",
#     @mock.patch("tested_app.my_microservice",
#                        side_effect=ValueError('Some Error'))
#     def test_200(self, mocker):
#         resp = self.app.get('/api')
#         print(type(self.app))
# #         print(FlaskClient.__module__ +  "." + FlaskClient.__name__)
#         print(self.app.__dir__())
#         from werkzeug.test import Client
# #         print(Client(self.app).__dir__())

# #         print(self.app)
# #         self.assertEqual(resp.status_code, 200)

    def test_404(self):
        resp = self.app.get('/non-existent')
        self.assertEqual(resp.status_code, 404)
        body = json.loads(resp.data.decode('utf-8'))
        self.assertEqual(body['code'], 404)
#         self.assertEqual(body['message'], '404: Not Found')
        self.assertEqual(body['description'], _404)


if __name__ == "__main__":
    unittest.main()
