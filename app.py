from flask import Flask, jsonify, abort
from werkzeug.exceptions import HTTPException, default_exceptions


def register_error_handlers(app):
    def error_handling(error):
        if isinstance(error, HTTPException):
            result = {'code': error.code,
                      'description': error.description,
                      'message': str(error)}
        else:
            description = abort.mapping[500].description
            result = {'code': 500,
                      'description': description,
                      'message': str(error)}

        resp = jsonify(result)
        resp.status_code = result['code']
        return resp

    for code in default_exceptions.keys():
        app.register_error_handler(code, error_handling)

    return app


app = Flask(__name__)
register_error_handlers(app)


@app.route('/api', methods=["POST", "DELETE", "GET"])
def my_microservice():
    response = jsonify({'Hello': 'World'})
    return response


@app.route('/api/person/<person_id>')
def person(person_id):
    response = jsonify({'Hello': person_id})
    return response


if __name__ == "__main__":
    app.run()
