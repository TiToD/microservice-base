import requests


class MyBugzilla:
    def __init__(
            self,
            account,
            server='https://bugzilla.mozilla.org'):

        self.account = account
        self.server = server
        self.session = requests.Session()

    def bug_link(self, bug_id):
        return "%s/show/bug.cgi?id=%s" % (self.server, bug_id)

    def get_new_bugs(self):
        call = self.server + '/rest/bug'
        params = {'assigned': self.account,
                  'status': 'NEW',
                  'limit': 10}
        try:
            res = self.session.get(call, params=params).json()
            print("=> Connection succesfull")
        except requests.exceptions.ConnectionError:
            print("!=> Connection Error occur!")
            res = {'bugs': [{"id": "0000"}]}

        self.links = []
        for bug in res['bugs']:
            id_ = (bug['id'])
            self.links.append(self.bug_link(id_))

        return self.links

# bug = MyBugzilla("zaem087@gmail.com")
# b = bug.get_new_bugs()
# print(b)

